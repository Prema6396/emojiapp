From node:16-alpine as build
WORKDIR ./
COPY . ./
RUN npm install
RUN npm run build

FROM nginx
COPY --from=build ./build* /usr/share/nginx/html/
